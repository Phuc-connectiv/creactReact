import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';
import IMAGE_1 from '../assets/construction-and-tools.png'

export default function Product (props) {
    const { product } = props;
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={IMAGE_1}/>
                <View>
                    <Text>
                        { product.name }
                    </Text>
                </View>
            </View>
        );
};
const styles = StyleSheet.create({
    container: {
    shadowColor: '#000',
    shadowOpacity: 0.5,
    shadowRadius: 10,
    shadowOffset: {width: 0, height: 0},
    backgroundColor: 'yellow',
    alignItems: 'center'
    },
    image: {
        width: 120,
        height: 120
    }
});