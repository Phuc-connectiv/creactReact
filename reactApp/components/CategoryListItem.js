import React from 'react';
import { Image, View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import ConstructionImage from '../assets/construction-and-tools.png';

export default function CategoryListItem(props) {
    const { category, onPress } = props;
    return (
        <TouchableOpacity activeOpacity={0.5} onPress={onPress}
           
        >
        <View style={styles.container}>
            <Text style={styles.title}>{category.title}</Text>
            <Image style={styles.categoryImage} source={ConstructionImage}/>
        </View>
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 16,
        borderRadius: 5,
        backgroundColor: '#fff',
        shadowColor: 'red',
        shadowOpacity: 0.9,
        shadowRadius: 10,
        shadowOffset: {width: 0, height: 0},
        marginBottom: 16
    },
    categoryImage: {
        width: 64,
        height: 64
    },
    title: {
        textTransform: 'uppercase',
        marginBottom: 8,
        fontWeight: 'bold',
        }
});