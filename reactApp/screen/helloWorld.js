import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native';

export default class HelloWorld extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.getParam('categoryName')
    };
  };
  render() {
    return (
      <TouchableOpacity activeOpacity={0.5} onPress={()=>{
        Alert.alert('kakakaka');
      }}>
      <View style={styles.container}>
        <Text style={styles.title}>Hello, world!!!</Text>
      </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      // flex: 1,
      // backgroundColor: '#fff',
      // alignItems: 'stretch',
      // justifyContent: 'center',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20,
      borderRadius: 4,
      backgroundColor: '#fff',
      padding: 10,
      marginLeft: 20,
      marginRight: 20,
      shadowColor: 'red',
      shadowOpacity: 1,
      shadowRadius: 50,
      shadowOffset: {width: 0, height: 0}
    },
    title: {
        fontWeight: 'bold',
        textTransform: 'uppercase'
    }
});