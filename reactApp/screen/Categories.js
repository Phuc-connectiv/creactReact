import React from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList } from 'react-native';
import CategoryListItem from '../components/CategoryListItem';

export default class Categories extends React.Component {
  static navigationOptions = {
    title: 'Home'
  }
  constructor(props){
    super(props);
    this.state = {
      categories: [],
      isLoading: true
    }
  }
  componentDidMount() {
    fetch('https://facebook.github.io/react-native/movies.json')
    .then((res) => res.json())
    .then((resJson) => {
      this.setState({
        isLoading: false,
        categories: resJson.movies
      })
    })
    .catch((err) => {console.log(err)});
  }

  render() {
    const { navigation } = this.props;
    const { categories } = this.state;
    if(this.state.isLoading){
      return <Text>Loading...</Text>
    } else {
      return (
        //---------- Flatlist ----------------------------------
                  <View>
                    <FlatList 
                      showsVerticalScrollIndicator
                      data={categories} 
                      renderItem={({item}) => 
                        <CategoryListItem 
                          category={item}
                          onPress={() => navigation.navigate('Category', {
                            categoryName: item.title
                          })}
                        />
                      }
                      keyExtractor={item => `${item.id}`}
                      contentContainerStyle={{paddingLeft: 16, paddingRight:16, paddingTop: 16}}
                    />
                  </View>
        
        //---------- Scroll View -------------------------------
                // <ScrollView style={{paddingLeft: 16, paddingRight: 16}} contentContainerStyle={{backgroundColor: 'yellow'}}>
                //   {categories.map((category) => {
                //     return <CategoryListItem key={category.id} category={category}/>
                //   })}
                // </ScrollView>
            );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    // display:'flex',
    // flexDirection: 'column',
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingLeft: 16,
    marginRight: 16
  },
});
