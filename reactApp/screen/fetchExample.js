import React from 'react';
import { Text, View, ActivityIndicator, FlatList } from 'react-native';

export default class FetchExample extends React.Component {
    constructor(props){
        super(props);
        this.state = { isLoading: true };
    }
    componentDidMount(){
        return fetch('https://facebook.github.io/react-native/movies.json')
            .then((res) => res.json())
            .then((resJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: resJson.movies
                }, function(){

                });
            })
            .catch((err) => console.log(err));
    }
    render() {
        if(this.state.isLoading){
            return (
                <View>
                    <ActivityIndicator/>
                </View>
            );
        }
        return (
            <View>
                <FlatList 
                    data={this.state.dataSource}
                    renderItem={({item}) => <Text>{item.title},{item.releaseYear}</Text>}
                    keyExtractor = {({id}, index) => id }/>
            </View>
        );
    }
}  