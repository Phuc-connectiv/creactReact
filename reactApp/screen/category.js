import React from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet
} from 'react-native';
import Product from '../components/product';

export default class Category extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('categoryName')
        };
    };
    state = {
        shop : [
            {id : 1, name: 'aaa'},
            {id : 2, name: 'bbb'}
        ]
    }
    render() {
        return (
            <FlatList 
                numColumns={2}
                contentContainerStyle={styles.content}
                data={this.state.shop}
                renderItem={({item}) => 
                    <View style={styles.container}>
                        <Product product={item}/>
                    </View>
                }
                keyExtractor={(item) => `${item.id}`}
            />
        );
    }
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 8
    },
    content: {
        paddingTop: 16,

        paddingHorizontal: 8
    }
});