import React, { Component } from 'react';
import { Text, View } from 'react-native';

class Blink extends Component {
    state = {
        isShow: true
    }
    componentDidMount() {
        setInterval(()=>(
            this.setState(previousState => (
                { isShow: !previousState.isShow }
              ))
        ),1000);
    }
    render() {
        if(!this.state.isShow){
            return null;
        }
        return(
            <Text>{this.props.text}</Text>
        );
    }
} 

export default class Flah extends React.Component {
    render(){
        return(
            <View>
                <Blink text='ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'/>
            </View>
        );
    }
} 
