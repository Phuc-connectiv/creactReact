import React, { Component } from 'react';
import { Text, View, FlatList, StyleSheet, TouchableOpacity, Alert } from 'react-native';

export default class ListView extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <FlatList 
                    data={[
                        {key: 'Aaa'},
                        {key: 'hAaa'},
                        {key: 'gfAaa'},
                        {key: 'cAbaa'},
                        {key: 'nAaa'},
                        {key: 'cAaa'},
                        {key: 'uAaa'},
                        {key: 'oaa'},
                        {key: 'iAaa'},
                        {key: 'lAaa'},
                        {key: 'tAaa'},
                        {key: 'wAaa'},
                    ]}
                renderItem={({item}) => <TouchableOpacity onPress={()=>{Alert.alert('Toi yeu em')}}><Text style={styles.item}>{item.key}</Text></TouchableOpacity>}/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
     flex: 1,
     paddingTop: 22
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
  })
  