import React from 'react';
import { SectionList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default class SectionListBasics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            array: [
                {title: 'a', data: ['a1', 'a2','a3','a4','a5','a6']},
                {title: 'b', data: ['b1', 'b2','b3','b4','b5','b6']}
            ]
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <SectionList
                sections={this.state.array}
                renderItem={({item})=> <TouchableOpacity><Text style={styles.item}>{item}</Text></TouchableOpacity>}
                renderSectionHeader={({section})=> <Text style={styles.sectionHeader}>{section.title}</Text>}
                keyExtractor={(item, index)=> index}
                />
            </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
})
