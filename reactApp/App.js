import React from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList } from 'react-native';
import AppNavigator from './navigation/AppNavigator';
import { createAppContainer } from 'react-navigation';

const AppContainer = createAppContainer(AppNavigator);
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
         <AppContainer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // display:'flex',
    // flexDirection: 'column',
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingLeft: 16,
    marginRight: 16
  },
});
