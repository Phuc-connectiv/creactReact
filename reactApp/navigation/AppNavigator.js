import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Categories from '../screen/Categories';
import HelloWorld from '../screen/helloWorld';
import Flah from '../screen/flah';
import Handling from '../screen/handling';
import Touchables from '../screen/touch';
import ListView from '../screen/listView';
import SectionListBasics from '../screen/selectList';
import FetchExample from '../screen/fetchExample';
import Category from '../screen/category';
import Card from '../screen/card';
import Order from '../screen/order';
import Setting from '../screen/setting';
import { FontAwesome5, FontAwesome } from '@expo/vector-icons';
import { Feather} from '@expo/vector-icons';
import { Icon } from '@expo/vector-icons';

const navigationhandler = ({navigation}) => ({header: null});
const CategoriesStack = createStackNavigator({
  Categories: {
    screen: Categories,
  },
  Category: {
    screen: Category,
  },
});
const CardStack = createStackNavigator({
  Card: {
    screen: Card,
  }
});
const SettingStack = createStackNavigator({
  Setting: {
    screen: Setting,
  }
});
const OrderStack = createStackNavigator({
  Order: {
    screen: Order,
  }
});
const AppNavigator = createBottomTabNavigator({
  Home: {
    screen: CategoriesStack,
    navigationOptions: {
      title: 'Home',
    }
  },
  Card: {
    screen: CardStack,
    navigationOptions: {
      title: 'Card',
    }
  },
  Order: {
    screen: OrderStack,
    navigationOptions: {
      title: 'Order',
    }
  },
  Setting: {
    screen: SettingStack,
    navigationOptions: {
      title: 'Setting',
      }
  }
});
export default AppNavigator;